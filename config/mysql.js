require('dotenv').config({ path: '.env' });

module.exports = () => {
    const mysql = require('mysql');
    let db;
    if (process.env.NODE_ENV == "development") {
        db = mysql.createPool({
            host: process.env.MYSQL_URL,
            user: process.env.MYSQL_USER,
            password: process.env.MYSQL_PWD,
            database: process.env.MYSQL_DB
        });
    } else {
        db = mysql.createPool(process.env.CLEARDB_DATABASE_URL);
    }

    global.db = db;
}