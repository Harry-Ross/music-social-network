const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const mysql = require('mysql');
const uuid = require('uuid');
const chalk = require('chalk')

function login(req, res, next) {
    const sql = mysql.format("SELECT id, name, password FROM users WHERE email = ?", [req.body.email]);

    db.query(sql, function(err, user) {
        if (err) {
            res.status(500).send({ success: false, message: "Internal server error"})
            return next(err);
        }
        if (user.length >= 1) {
            bcrypt.compare(req.body.password, user[0].password, function (err, result) {
                if (err) {
                    res.status(401).send({ success: false, message: "Incorrect password"})
                    return next(err);
                }
                const token = generateToken(user[0].id, user[0].name, req.body.email);
                res.status(200).send({
                    name: user[0].name,
                    token 
                });
            })
        } else {
            res.status(401).send({ success: false, message: "User not found" });
            return next()
        }
    })
}

function register(req, res, next) {
    const user_uuid = uuid.v4();

    // TODO: Much cleaner way to output the request body
    var sql = mysql.format("INSERT INTO users (id, name, email, password) VALUES (?, ?, ?, ?)", 
    [
        user_uuid,
        req.body.name, 
        req.body.email,
        hashPassword(req.body.password)
    ]);

    db.query(sql, function(err, result) {
        if (err) {
            res.status(401).send({ success: false, error: "Registration failed" });
            console.log(chalk.blue('yooo'))
            return next(err);
        } else {
            const token = generateToken(user_uuid, req.body.name, req.body.email);
            res.status(200).send({ success: true, token })
        }
    })
}

function generateToken(user_uuid, name, email) {
    return jwt.sign({ id: user_uuid, name, email }, process.env.JWT_SECRET, { expiresIn: '24h' });
}

function hashPassword(password) {
    try {
        let hash = bcrypt.hashSync(password, 10);
        return hash;
    } catch (err) {
        console.error(err);
    }
}

module.exports = {
    register, login
};