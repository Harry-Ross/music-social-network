const jwt = require('jsonwebtoken');

const lastfm = "53522b09fdf5339fb865b0d38eb400f9";

function sendLastfm(req, res, next) {
    const token = req.headers.token;
    
    jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
        if (err) {
            res.status(401).send({ success: false });
            return next(err);
        }
        res.status(200).send({ success: true, key: lastfm });
    })
}

module.exports = sendLastfm;