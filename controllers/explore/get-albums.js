module.exports = function(req, res, next) {
    res.status(200).send(
        { 
            success: true,
            albums : [
                {
                    name: "Currents",
                    artist: "Tame Impala",
                    albumart: "https://upload.wikimedia.org/wikipedia/en/9/9b/Tame_Impala_-_Currents.png",
                    listeners: 14
                },
                {
                    name: "DAMN.",
                    artist: "Kendrick Lamar",
                    albumart: "https://upload.wikimedia.org/wikipedia/en/5/51/Kendrick_Lamar_-_Damn.png",
                    listeners: 8
                },
                {
                    name: "Evil Empire",
                    artist: "Rage Against the Machine",
                    albumart: "https://upload.wikimedia.org/wikipedia/en/4/45/Rage_Against_the_Machine_-_Evil_Empire.png",
                    listeners: 15
                },
                {
                    name: "Fresh Fruit for R",
                    artist: "Dead Kennedys",
                    albumart: "https://upload.wikimedia.org/wikipedia/en/2/22/Dead_Kennedys_-_Fresh_Fruit_for_Rotting_Vegetables_cover.jpg",
                    listeners: 12
                },
                {
                    name: "The Fool",
                    artist: "Bladee",
                    albumart: "https://e.snmc.io/i/600/w/382df1bddf907bf0c9a33fd7ef8c1f4d/9049057/bladee-the-fool-Cover-Art.jpg",
                    listeners: 20
                },
            ]
        }
    )
}