const mysql = require('mysql');
const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    let sql = mysql.format('SELECT users.id, users.name, users.email, users.profile_img, users.admin, followers.* FROM users LEFT JOIN (SELECT user_id, COUNT(user_id) FROM users_following GROUP BY user_id ORDER BY count(user_id) DESC) followers ON users.id=followers.user_id;');
    db.query(sql, (err, result) => {
        if (err) {
            res.status(401).send({ success: false });
            return next(err);
        }
        const data = result.map(user => ({ 
            user_id: user.id,
            name: user.name,
            email: user.email,
            profile_img: user.profile_img,
            admin: user.admin,
            followers: user['COUNT(user_id)'] || 0
        }))
        res.status(200).send({
            success: true,
            users: data
        })
    })
}