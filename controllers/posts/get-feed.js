const mysql = require('mysql');
const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    const token = req.headers.token;

    jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
        if (err) {
            res.status(401).send({ success: false });
            return next(err);
        } 
        let sql = mysql.format('SELECT users.name, users.profile_img, t.user_id, t.id, t.content, t.album, t.artist, t.rating, t.posted FROM users JOIN (SELECT posts.id, posts.user_id, posts.content, posts.album, posts.artist, posts.rating, posts.posted  FROM users_following JOIN posts ON users_following.following_id=posts.user_id WHERE users_following.user_id=?) as t ON t.user_id=users.id ORDER BY t.posted DESC', decoded.id);
        db.query(sql, (err, result) => {
            if (err) {
                res.status(500).send({ success: false, err });
                console.error(err);
                return next(err);
            }
            res.status(200).send({ success: true, posts: result })
        })
    })
}