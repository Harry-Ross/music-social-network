const mysql = require('mysql');
const jwt = require('jsonwebtoken');
const uuid = require('uuid');


module.exports = function (req, res, next) {
    const token = req.headers.token;
    
    jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
        if (err) {
            res.status(401).send({ success: false });
            return next(err);
        } 
        console.log(req.body.rating)
        const user_id = decoded.id;
        let sql = mysql.format('INSERT INTO posts (user_id, id, artist, album, content, rating) VALUES (?, ?, ?, ?, ?, ?)', [
            user_id,
            uuid.v4(),
            req.body.artist,
            req.body.album,
            req.body.content,
            req.body.rating
        ]);
        db.query(sql, (err, result) => {
            if (err) {
                res.status(500).send({ success: false });
                return next(err);
            }
            res.status(200).send({ success: true, message: result.message });
        })
    })
}