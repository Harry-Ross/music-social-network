const jwt = require('jsonwebtoken');
const mysql = require('mysql');

module.exports = function (req, res, next) {
    const token = req.headers.token;

    jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
        if (err) {
            res.status(401).send({ success: false });
            return next(err);
        } 
        const query = req.query.q;
        let sql = mysql.format('SELECT id, name, artist_id FROM albums WHERE name LIKE ?', '%' + query + '%');
        db.query(sql, (err, result) => {
            if (err) {
                res.status(500).send({ success: false });
                return next(err);
            }
            res.status(200).send({ success: true, posts: result });
        })
    });
}