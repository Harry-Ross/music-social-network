const mysql = require('mysql');

module.exports = function (req, res, next) {
    const { artist, album } = req.params;

    let sql = mysql.format('SELECT id, rating, content, user_id, posted FROM posts WHERE artist=? AND album=?', [artist, album]);
    console.log(sql);
    
    db.query(sql, (err, result) => {
        if (err) {
            res.status(500).send({ success: false });
            console.error(err);
            return next(err);
        }
        res.status(200).send({ 
            success: true,
            listener_count: result.length,
            comments: result
        });
    })
}