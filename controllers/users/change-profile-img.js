const mysql = require('mysql');
const jwt = require('jsonwebtoken');

module.exports = function(req, res, next) {
    const token = req.headers.token;

    jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
        if (err) {
            res.status(401).send({ success: false });
            console.error(err)
            return next(err);
        } 
        sql = mysql.format('UPDATE users SET profile_img=? WHERE id=?',[req.body.profile_img, decoded.id]);
        db.query(sql, (err, result) => {
            if (err) {
                res.status(500).send({ success: false });
                console.error(err);
                return next(err);
            }
            res.status(200).send({
                success: true,
                changed: true
            })
        })
    })
}