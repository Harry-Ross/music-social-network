const mysql = require('mysql');
const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    const token = req.headers.token;
    
    jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
        if (err) {
            res.status(401).send({ success: false, message: "Token invalid" });
            console.error(err);
            return next(err);
        } 

        let sql = mysql.format('SELECT users.name, users.email, users.profile_img, users.created, users.admin, posts.id, posts.artist, posts.album, posts.rating, posts.content, posts.posted, posts.user_id FROM users JOIN posts ON users.id=posts.user_id WHERE users.id=? ORDER BY posts.posted DESC;', req.params.id);
        db.query(sql, (err, result) => {
            if (err) {
                res.status(500).send({ success: false, err });
                console.error(err);
                return next(err);
            }
            if (result.length === 0) {
                res.status(400).send({ success: false })
                return next();
            }
            let posts = result.map(post => ({
                id: post.id,
                artist: post.artist,
                album: post.album,
                rating: post.rating,
                content: post.content,
                user_id: post.user_id
            }))
            const user_result = result;
            
            sql = mysql.format('SELECT following_id FROM users_following WHERE user_id = ? UNION ALL SELECT user_id FROM users_following WHERE following_id = ?', [req.params.id, req.params.id]);
            db.query(sql, (err, result) => {
                if (err) {
                    res.status(500).send({ success: false, err });
                    console.error(err);
                    return next(err);
                }
                console.log(user_result[0].id)
                const following_result = result.map(item => item.following_id).filter(e=>e);
                const followers_result = result.map(item => item.user_id).filter(e=>e)
                
                const data = {
                    id: req.params.id,
                    name: user_result[0].name,
                    email: user_result[0].email,
                    profile_img: user_result[0].profile_img,
                    created: user_result[0].created,
                    admin: user_result[0].admin,
                    posts: posts,
                    following: following_result,
                    followers: followers_result,
                    following_user: followers_result.includes(decoded.id)
                }
                res.send({
                    success: true,
                    user: data
                })
            })
        })
    })
}