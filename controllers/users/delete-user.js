const mysql = require('mysql');
const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    const token = req.headers.token;

    jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
        if (err) {
            res.status(401).send({ success: false });
            return next(err);
        } 
        let sql = mysql.format('SELECT admin FROM users WHERE id = ?', decoded.id);
        db.query(sql, (err, result) => {
            if (result[0].admin === 1) {
                const id = req.params.id;
                sql = mysql.format("DELETE FROM users WHERE id=?", id);
                db.query(sql, (err) => {
                    if (err) {
                        res.status(500).send({ success: false, error: err })
                        next(err);
                    } 
                    sql = mysql.format("DELETE FROM posts WHERE user_id=?", id)
                    db.query(sql, (err) => {
                        if (err) {
                            res.status(500).send({ success: false, error: err })
                            next(err);
                        } 
                        res.status(200).send({ success: true, id })
                    })
                })
            } else {
                res.status(401).send({ success: false, message: "Not admin" })
                return next();
            }
        })
    })
}