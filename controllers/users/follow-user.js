const jwt = require('jsonwebtoken');
const mysql = require('mysql');

module.exports = function(req, res, next) {
    const token = req.headers.token;

    jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
        if (err) {
            res.status(401).send({ success: false });
            return next(err);
        } 
        if (decoded.id === req.params.user_id) {
            res.status(400).send({
                success: false,
                message: "You cannot follow your own account"
            })
            return next();
        }
        let sql = mysql.format('SELECT COUNT(*) FROM users_following WHERE user_id = ? AND following_id = ?', [decoded.id, req.params.user_id])
        db.query(sql, (err, result) => {
            if (err) {
                res.status(500).send({ success: false });
                console.error(err);
                return next(err);
            }
            if (result.length === 0) {
                sql = mysql.format('INSERT INTO users_following (user_id, following_id) VALUES (?, ?)', [decoded.id, req.params.user_id]);
                db.query(sql, (err, result) => {
                    if (err) {
                        res.status(500).send({ success: false });
                        console.error(err);
                        return next(err);
                    }
                    res.status(200).send({
                        success: true,
                        followed: true
                    })
                })
            } else {
                sql = mysql.format('DELETE FROM users_following WHERE user_id = ? AND following_id = ?', [decoded.id, req.params.user_id]);
                db.query(sql, (err, result) => {
                    if (err) {
                        res.status(500).send({ success: false });
                        console.error(err);
                        return next(err);
                    }
                    res.status(200).send({
                        success: true,
                        followed: false
                    })
                })
            }
        })
    })
}