require('dotenv').config({ path: '.env' });
const chalk = require('chalk');
global.chalk = chalk;

const app = require('./config/express-app');
let routes = require('./routes/index')
app.use('/', routes);

require('./config/mysql')()

const port = process.env.PORT || 4000;
app.listen(port, () => { 
    console.log(`Running on port ${port}`); 
})