const express = require('express');
const app = express.Router();

const getUser = require('../../controllers/users/get-user')
app.get('/user/:id', getUser);

const followUser = require('../../controllers/users/follow-user');
app.post('/follow/:user_id', followUser);

const deleteUser = require('../../controllers/users/delete-user');
app.post('/user/delete/:id', deleteUser);

const changeProfileImg = require('../../controllers/users/change-profile-img');
app.post('/user/profile_img', changeProfileImg);

module.exports = app;