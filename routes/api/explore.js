const express = require('express');
const app = express.Router();

const getUsers = require('../../controllers/explore/get-users');
app.get('/users', getUsers);

const getAlbums = require('../../controllers/explore/get-albums');
app.get('/albums', getAlbums)

module.exports = app;