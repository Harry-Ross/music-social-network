const express = require('express');
const app = express.Router();

const rateLimit = require('express-rate-limit');
const createPostLimit = rateLimit({
    windowMs: 15 * 60 * 1000,
    max: 15,
    message: "Too many posts"
})

const getPost = require('../../controllers/posts/get-post')
app.get('/post/:id', getPost);

const createPost = require('../../controllers/posts/create-post');
app.post('/post/new', createPostLimit, createPost);

const getFeed = require('../../controllers/posts/get-feed');
app.get('/feed', getFeed);

module.exports = app;