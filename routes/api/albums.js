const express = require('express');
const app = express.Router();

const getAlbum = require('../../controllers/albums/get-album');
app.get('/:artist/:album', getAlbum);

const albumSearch = require('./../../controllers/albums/album-search');
app.get('/search', albumSearch);

module.exports = app;