const express = require('express');
const app = express.Router();

const rateLimit = require('express-rate-limit');

const registerLimiter = rateLimit({
    windowMs: 60 * 60 * 1000,
    max: 5,
    message: "Too many accounts created on this IP address"
})

const authMethods = require('../../controllers/auth/auth-methods');

app.post('/register', registerLimiter, authMethods.register);
app.post('/login', authMethods.login);

const lastfm = require('../../controllers/auth/lastfm-sender');
app.get('/lastfm', lastfm);

module.exports = app;