const express = require('express');
const app = express.Router();

const authRoutes = require('./api/auth');
app.use('/api/auth', authRoutes);

const albumRoutes = require('./api/albums');
app.use('/api/albums', albumRoutes);

const postRoutes = require('./api/posts');
app.use('/api', postRoutes);

const userRoutes = require('./api/users');
app.use('/api', userRoutes);

const exploreRoutes = require('./api/explore');
app.use('/api/explore', exploreRoutes);

module.exports = app;